<?php

/**
 * @file
 * This file implements the gdfsuezmobile service resource call back functions.
 */

/**
 * Determines whether the current user can access a gdfsuezmobile resource.
 *
 * @param string $op
 *   String indicating which operation to check access for.
 * @param array $args
 *   Array arguments passed through from the original request.
 *
 * @return boolean
 *   Boolean indicating whether or not the user has access to the resource.
 *
 * @see node_access()
 */
/*function _webservices_resource_access($op = 'view', $args = array()) {
  return user_access($op);
}*/

/**
 * Performs a user login service resource call and bundles up the gdfsuezmobile
 * system connect resource results as well.
 *
 * @param $username
 *   String The Drupal user name.
 * @param $password
 *   String The Drupal user password.
 *
 * @return array
 *   Array with the user login result and gdfsuezmobile system connect result.
 */
function _gdfsuezmobile_resource_user_login($username, $password) {
  $results = array();

  $results = _user_login($username, $password);

  return $results;
}

function _webservices_resource_content_getAll() {
  global $user;

  // Je récupère les Catégories et les Lois associés.
  $terms = taxonomy_get_tree(2);

  foreach ($terms as $term) {
    $nodes = taxonomy_select_nodes($term->tid);

    $node_all = array();
    foreach ($nodes as $node) {
      $node_array = array();
      $node_wrapper = entity_metadata_wrapper('node', $node);
      $node_array["nid"] = $node_wrapper->nid->value();
      $node_array["title"] = $node_wrapper->title->value();
      $node_array["link"] = url("node/" . $node_wrapper->nid->value(), array("absolute" => TRUE));
      $node_array["body"] = $node_wrapper->body->value()["value"];
      $node_array["categorie_name"] = $term->name;
      $node_array["changed"] = $node_wrapper->changed->value();
      $node_array["ressources"] = $node_wrapper->field_lois_ressources->value();
      $node_array["comments_count"] = intval($node_wrapper->comment_count->value());


      $criteria = array(
        "entity_id" => $node_wrapper->nid->value(),
        "entity_type" => "node",
        "value_type" => "option",
        "tag" => "vote",
      );
      $votes = call_user_func("votingapi_select_results", $criteria, 0);
      /*$node_array["vote_moins"] = $vote;
      $node_array["vote_plus"] = $vote;*/

      $node_array["vote_moins"] = $node_array["vote_plus"] = 0;
      foreach ($votes as $vote) {


        /*  if ($vote["function"] == "option--1") {
            $node_array["vote_moins"] = intval($vote["value"]);
          }
          else {*/
        if ($vote["function"] == "option-1") {
          $node_array["vote_plus"] = intval($vote["value"]);
        }
        //}

      }

      $node_all[] = $node_array;

      $term->lois = $node_all;

      // watchdog('nodes title', "<pre>" . print_r($node_wrapper->title->value(), TRUE) . "</pre>");

      unset($term->parents);
      unset($term->description);
      unset($term->weight);
      unset($term->depth);
      unset($term->format);
      unset($term->vid);
    }
  }

  return array("categories" => $terms);

}

function _webservices_resource_content_getFavorites() {

  global $user;

  $query = db_select('field_data_field_user_favoris', 'user_favoris');
  $query
    ->fields('user_favoris', array('field_user_favoris_value'))
    ->condition('entity_id', array($user->uid), 'IN');
  $result = $query->execute();
  $num_of_results = $result->rowCount();

  if ($num_of_results) {
    //Je loade les entity.
    $array_of_ids = [];
    foreach ($result as $record) {
      $array_of_ids[$record->field_user_favoris_value] = $record->field_user_favoris_value;
    }

    //Je loade les entités
    $favoris = entity_load('field_collection_item', $array_of_ids);

    //Je construit la réponse.
    $reponse = [];
    foreach ($favoris as $favori) {
      $currentItem = [];
      $item = entity_metadata_wrapper('field_collection_item', $favori);
      $currentItem["date_ajout"] = format_date($item->field_fc_favoris_date_ajout->value(), 'tres_court');
      //$currentItem["fid"] = $item->item_id->value();
      $currentItem["nid"] = $item->field_fc_favoris_lois->value()->nid;
      $reponse[] = $currentItem;
    }

    //watchdog('creneaux', "<pre>" . print_r($favoris, TRUE) . "</pre>");
    return array("favoris" => $reponse);
  }
  else {
    return array("favoris" => array());
  }
}

function _webservices_resource_content_addFavorites($nid) {
  global $user;
  $connectedUser = user_load($user->uid);
  $date_ajout = date("Y-m-d H:i:s");

  $values = array();
  $values['field_name'] = 'field_user_favoris'; // field collection name
  $values['field_fc_favoris_lois'][LANGUAGE_NONE][0]['nid']['number'] = $nid;
  $values['field_fc_favoris_date_ajout'][LANGUAGE_NONE][0]['value'] = $date_ajout;

  $field_collection_value = entity_create('field_collection_item', $values);
  $field_collection_value->setHostEntity('user', $connectedUser);
  $field_collection_value->save();

  return array("success" => $date_ajout);
}

function _webservices_resource_content_deleteFavorites($nid) {
  global $user;

  $query = db_select('field_data_field_fc_favoris_lois', 'user_favoris');
  $query->join('field_data_field_user_favoris', 'uf', 'user_favoris.entity_id = uf.field_user_favoris_value');
  $query
    ->fields('user_favoris', array('entity_id'))
    ->condition('user_favoris.field_fc_favoris_lois_nid', $nid, '=')
    ->condition('uf.entity_id', $user->uid, '=');
  $result = $query->execute();
  $num_of_results = $result->rowCount();
  if ($num_of_results) {
    //Je loade les entity.
    $array_of_ids = [];
    foreach ($result as $record) {
      $array_of_ids[$record->entity_id] = $record->entity_id;
    }

    //Je loade les entités
    $favoris = entity_load('field_collection_item', $array_of_ids);

    //Je construit la réponse.
    foreach ($favoris as $favori) {
      $item = entity_metadata_wrapper('field_collection_item', $favori);
      $item->delete();
    }

    //watchdog('creneaux', "<pre>" . print_r($favoris, TRUE) . "</pre>");
    return array("success" => TRUE);
  }
  else {
    return array("error" => TRUE);
  }
}

function _webservices_resource_current_user_comments() {
  global $user;

  $query = db_select('comment', 'c');
  $query
    ->fields('c', array('cid'))
    ->condition('c.status', 1)
    ->condition('c.uid', $user->uid);
  $result = $query->execute();

  $array_of_comments_id = [];
  foreach ($result as $comments) {
    $array_of_comments_id[$comments->cid] = $comments->cid;
  }

  $comments = entity_load('comment', $array_of_comments_id);

  $all_comments = [];
  foreach ($comments as $comment) {

    $lois = node_load($comment->nid);

    $current_comment = [
      "cid" => $comment->cid,
      "title_lois" => $lois->title,
      "date_creation" => format_date($comment->created, 'tres_court'),
      "body" => $comment->comment_body["und"][0]["safe_value"],
      "nid" => $comment->nid,
    ];

    $all_comments[] = $current_comment;
  }

  return array("comments" => $all_comments);
}

/**
 * Returns the comments of a specified node.
 *
 * @param $nid
 *   Unique identifier for the node.
 * @param $count
 *   Number of comments to return.
 * @param $start
 *   Which comment to start with. If present, $start and $count are used together
 *   to create a LIMIT clause for selecting comments. This could be used to do paging.
 * @return
 *   An array of comment objects.
 */
function _webservices_resource_node_comments($nid, $count = 0, $start = 0) {
  $query = db_select('comment', 'c');
  $query->innerJoin('node', 'n', 'n.nid = c.nid');
  $query->addTag('node_access');
  $query->fields('c', array('cid'))
    ->condition('c.nid', $nid);

  if ($count) {
    $query->range($start, $count);
  }

  $result = $query->execute()
    ->fetchAll();

  foreach ($result as $record) {
    $cids[] = $record->cid;
  }
  if (!empty($cids)) {
    $comments = entity_load('comment', $cids);

    $all_comments = [];
    $parents_comments = [];
    $enfant_comments = [];
    foreach ($comments as $comment) {

      $criteria = array(
        "entity_id" => $comment->cid,
        "entity_type" => "comment",
        "value_type" => "option",
        "tag" => "vote",
      );
      $votes = call_user_func("votingapi_select_results", $criteria, 0);

      $parent = 0;
      if (isset($comment->pid)) {
        $parent = $comment->pid;
      }

      /* $autor = user_load($comment->uid);

       $autor_wrapper = entity_load("user", $autor);*/

      $current_comment = [
        "nid" => $comment->nid,
        "cid" => $comment->cid,
        "date_creation" => format_date($comment->created, 'tres_court_heure'),
        "body" => $comment->comment_body["und"][0]["safe_value"],
        "avatar" => "avatar",
        "prenom_nom" => "Prenom nom",
        "parent" => $parent,
      ];

      $current_comment["vote_moins"] = $current_comment["vote_plus"] = 0;
      foreach ($votes as $vote) {


        if ($vote["function"] == "option--1") {
          $current_comment["vote_moins"] = intval($vote["value"]);
        }
        else {
          if ($vote["function"] == "option-1") {
            $current_comment["vote_plus"] = intval($vote["value"]);
          }
        }

      }

      if ($parent == 0) {
        $parents_comments[] = $current_comment;
      }
      else {
        $enfant_comments[] = $current_comment;
      }

      $all_comments[] = $current_comment;
    }

    // J'ai la liste de tous les commentaire. Maintenant je dois les renvoyer en respectant la hierarchie parent enfant classé par nombre de vote.
    // Je boucle parmis les enfants pour voir si un commentaire n'est pas le fils d'un autre
    foreach ($enfant_comments as $key => $enfant_comment) {
      foreach ($enfant_comments as $compare_enfant) {
        if ($enfant_comment["parent"] == $compare_enfant["cid"]) {
          $enfant_comments[$key]["parent"] = $compare_enfant["parent"];
          break;
        }
      }
    }

    foreach ($enfant_comments as $key => $row) {
      $vote_value[$key] = $row["vote_plus"];
      $date_value[$key] = $row["cid"];
    }
    array_multisort($vote_value, SORT_DESC, $date_value, SORT_DESC, $enfant_comments);

    foreach ($parents_comments as $key => $row) {
      $vote_value[$key] = $row["vote_plus"];
      $date_value[$key] = $row["cid"];
    }
    array_multisort($vote_value, SORT_DESC, $date_value, SORT_DESC, $parents_comments);


    // Je boucle parmis les parents et les enfants, je crée une 3ème tableau nikel.
    $comments_all_sorted = [];
    $index = 0;
    foreach ($parents_comments as $parent) {
      $parent["index"] = $index;
      $comments_all_sorted[] = $parent;
      $index++;
      foreach ($enfant_comments as $key => $enfant_comment) {
        if ($parent["cid"] == $enfant_comment["parent"]) {
          $enfant_comment["index"] = $index;
          $comments_all_sorted[] = $enfant_comment;
          $index++;
          unset($enfant_comments[$key]);
        }
      }
    }

    return array("comments" => $comments_all_sorted);
  }
  else {
    return array("comments" => array());
  }
}


function _webservices_resource_twitter_connectUser($twitterAccount) {

  $twitter_id = $twitterAccount["twitter_id"];
  $twitter_key = $twitterAccount["key"];
  $twitter_secret = $twitterAccount["secret"];
  $twitter_username = $twitterAccount["twitter_username"];
  $twitter_email = $twitterAccount["twitter_email"];

  $path = drupal_get_path('module', 'twitter');
  require_once $path . '/twitter.inc';

  // $password = "01@)°)!/loisutiles-_é3&1";
  //return array("toto");
  if ($uid = db_query("SELECT uid FROM {twitter_account} WHERE twitter_uid = :twitter_uid",
    array(':twitter_uid' => $twitter_id))->fetchField()
  ) {
    // We have an existing Twitter account - set it up for login.
    $user = user_load($uid);
    return connectUser($user->uid);
    /*    return array("user" => $user->name, "password" => $password);
        /*$edit["authname_twitter"] = $twitter_id;
        user_save($account, $edit);
        $user = $account;
        $form_state['twitter_oauth']['account'] = $account;
        $success = TRUE;*/
  }
  else {
    // No existing user account, let's see if we can register.
    // Check for a nickname collision.
    $account = array_shift(user_load_multiple(array(), array('name' => $twitter_username)));
    if (empty($account->uid)) {

      //require './includes/password.inc';

      $password = user_password(8);

      $account->name = $twitter_username;
      $account->pass = $password;
      $account->init = $twitter_username;
      $account->email = $twitter_email;
      $account->status = 1;
      $account->authname_twitter = $twitter_id;
      $account->access = REQUEST_TIME;
      $account->is_new = TRUE;
      $account = user_save($account);

      $user = $account;
      //$form_state['twitter_oauth']['account'] = $account;

      $key = variable_get('twitter_consumer_key', '');
      $secret = variable_get('twitter_consumer_secret', '');

      $twitter = new Twitter($key, $secret, $twitter_key, $twitter_secret);
      try {
        $twitter_account = $twitter->users_show($twitter_username);
      }
      catch (TwitterException $e) {
        form_set_error('screen_name', t('Request failed: @message.', array('@message' => $e->getMessage())));
        return;
      }

      $response = array(
        "oauth_token" => $twitter_key,
        "oauth_token_secret" => $twitter_secret,
        "user_id" => $twitter_id,
        "screen_name" => $twitter_username,
      );

      $twitter_account->set_auth($response);
      $twitter_account->uid = $user->uid;
      twitter_account_save($twitter_account, TRUE);

      return connectUser($twitter_account->uid);

      //return array("user" => $user->name, "password" => $password);
    }
    else {
      return array("erreur");
      /*drupal_set_message(t('The nickname %name is already in use on this site, please register below with a new nick name, or @login to continue.',
        array(
          '%name' => $twitter_username,
          '@login' => url('user/login')
        )
      ), 'warning');*/
    }
  }
}

function _webservices_resource_facebook_connectUser($fb_account) {

  $fb_id = $fb_account["fb_id"];
  $fb_firstname = $fb_account["fb_firstname"];
  $fb_lastname = $fb_account["fb_lastname"];
  $fb_username = $fb_account["fb_username"];
  $fb_email = $fb_account["fb_email"];
  $fb_token = $fb_account["fb_token"];

  $fb_id = intval($fb_id);
  $query = db_select('users', 'u');
  $query->condition('u.mail', check_plain($fb_email));
  $query->fields('u', array('uid'));
  /* $query->leftJoin('field_data_field_user_fb_token', 'fb_token', 'u.uid = fb_token.entity_id');
   $query->condition('fb_token.field_user_fb_token_value', $fb_token, '=');*/
  $query->range(0, 1);

  $drupal_user_id = 0;
  $result = $query->execute();
  foreach ($result as $record) {
    $drupal_user_id = $record->uid;
  }


  if ($drupal_user_id) {
    $user = user_load($drupal_user_id);
    if ($user->uid && in_array('authenticated user', $user->roles)) {
      return connectUser($user->uid);
    }
    else {
      return ("Compte pas encore activé");
    }
  }
  //il n'existe pas on le crée et on le fait se connecter
  else {

    $drupal_username_generated = simple_fb_connect_unique_user_name(check_plain($fb_username));

    $password = $password = user_password(8);

    $new_user = array(
      'name' => $drupal_username_generated,
      'pass' => $password,
      'mail' => $fb_email,
      'status' => 1,
      'field_membre_prenom' => array('und' => array(0 => array('value' => $fb_firstname))),
      'field_membre_nom' => array('und' => array(0 => array('value' => $fb_lastname))),
      'field_user_fb_token' => array('und' => array(0 => array('value' => $fb_token))),
      //'fbuid' => $fb_id,
      'init' => 'email address',
      'roles' => array(
        DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      ),
    );

    $dimensions_in_text = variable_get('user_picture_dimensions', SIMPLE_FB_CONNECT_DEFAULT_DIMENSIONS_STRING);
    $dimensions = explode('x', $dimensions_in_text);
    if (count($dimensions) == 2) {
      $width = $dimensions[0];
      $height = $dimensions[1];
    }
    else {
      $width = SIMPLE_FB_CONNECT_DEFAULT_WIDTH;
      $height = SIMPLE_FB_CONNECT_DEFAULT_HEIGHT;
    }
    $pic_url = "https://graph.facebook.com/" . check_plain($fb_id) . "/picture?width=$width&height=$height";
    $response = drupal_http_request($pic_url);
    $file = 0;
    if ($response->code == 200) {
      $picture_directory = file_default_scheme() . '://' . variable_get('user_picture_path', 'pictures/');
      file_prepare_directory($picture_directory, FILE_CREATE_DIRECTORY);
      $file = file_save_data($response->data, $picture_directory . '/' . check_plain($fb_id . '.jpg', FILE_EXISTS_RENAME));
    }
    if (is_object($file)) {
      $new_user['picture'] = $file->fid;
    }

    $data = user_save(NULL, $new_user);
    return connectUser($data->uid);
  }
}

/*
 * Cette fonction est le point central des creations/connection
 * Elle peut recevoir une demande de creation/connection depuis les 3 types de compte (drupal,fb,twitter).
 */
function _webservices_resource_current_user_createlogin($account) {
  $type = $account["type"];
  $object = $account["account"][0];

  //Je fais une demande de creation avec un compte purement drupal.
  if ($type == "drupal") {
    $data = creerUser($object);
    return connectUser($data["uid"]);
  }
  else {
    if ($type == "fb") {
      return _webservices_resource_facebook_connectUser($object);
    }
    else {
      if ($type == "twitter") {
        return _webservices_resource_twitter_connectUser($object);
      }
    }
  }
}

function creerUser($object) {
  $path = drupal_get_path('module', 'services');
  require_once $path . '/includes/services.runtime.inc';
  require_once $path . '/resources/user_resource.inc';

  return $data = _user_resource_create($object);
}

function connectUser($uid) {

  $path = drupal_get_path('module', 'services');
  require_once $path . '/includes/services.runtime.inc';
  require_once $path . '/resources/user_resource.inc';

  $form_state['uid'] = $uid;
  user_login_submit(array(), $form_state);


  $return = new stdClass();
  $return->sessid = session_id();
  $return->session_name = session_name();
  $return->token = drupal_get_token('services');

  $user = user_load($uid);
  $account = clone $user;

  services_remove_user_data($account);

  $return->user = $account;

  return $return;
}

