<?php
//kpr(get_defined_vars());
//kpr($theme_hook_suggestions);
//template naming
//page--[CONTENT TYPE].tpl.php
?>
<?php if (theme_get_setting('mothership_poorthemers_helper')) { ?>
  <!-- page.tpl.php -->
<?php } ?>

<?php print $mothership_poorthemers_helper; ?>

<?php include("header.php"); ?>

<div class="page">

  <div role="main" id="main-content">
    <?php /*if ($title): */ ?><!--
      <h1><?php /*print $title; */ ?></h1>
    --><?php /*endif; */ ?>

    <?php /*print $breadcrumb; */ ?>

    <?php if ($action_links): ?>
      <ul class="action-links"><?php print render($action_links); ?></ul>
    <?php endif; ?>

    <?php /*if (isset($tabs['#primary'][0]) || isset($tabs['#secondary'][0])): */ ?><!--
      <nav class="tabs"><?php /*print render($tabs); */ ?></nav>
    --><?php /*endif; */ ?>

    <?php if ($page['highlighted'] OR $messages) { ?>
      <div class="drupal-messages">
        <?php print render($page['highlighted']); ?>
        <?php print $messages; ?>
      </div>
    <?php } ?>

    <div class="row box-greenlight">
      <div class="c-6">
        <div class="box-content">
          <h2 class="heading-2">Connectez-vous avec Facebook / Twitter</h2>

          <p>En vous connectant, vous pourrez ajouter les lois en favoris, soumettre des lois utiles et profiter des nouveautés à venir sur Lois Utiles.</p>
          <?php
          print l("Se connecter avec Facebook", $GLOBALS['base_url'] . "/user/simple-fb-connect");

          print l("Se connecter avec Twitter", $GLOBALS['base_url'] . "/twitter/redirect");

          ?>
          <?php print render($page['facebook_connect']); ?>
        </div>
      </div>
      <div class="c-12">
			<span class="choice purple">
				ou
			</span>
      </div>
      <div class="c-6">
        <div class="box-content">
          <?php print render($page['content']); ?>
        </div>
      </div>
    </div>


  </div>
  <!-- /main -->

  <?php if ($page['sidebar_first']): ?>
    <div class="sidebar-first">
      <?php print render($page['sidebar_first']); ?>
    </div>
  <?php endif; ?>

  <?php if ($page['sidebar_second']): ?>
    <div class="sidebar-second">
      <?php print render($page['sidebar_second']); ?>
    </div>
  <?php endif; ?>
</div><!-- /page -->

<footer role="contentinfo">
  <?php print render($page['footer']); ?>
</footer>

