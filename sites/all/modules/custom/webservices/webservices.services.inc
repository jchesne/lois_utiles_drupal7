<?php

/**
 * @file
 * gdfsuezmobile's implementation of Hooks provided by Services
 * for the definition of new service resources.
 */


/**
 * Defines function signatures for resources available to services.
 */
function webservices_services_resources() {
  $webservices_resource = array(
    'contents' => array(
      'actions' => array(
        'getAll' => array(
          'help' => t('Récupère toutes les categories et lois de l\'application.'),
          'file' => array(
            'type' => 'inc',
            'module' => 'webservices',
            'name' => 'webservices.resource',
          ),
          'access arguments' => array('access content'),
          'callback' => '_webservices_resource_content_getAll',
        ),
        'getComments' => array(
          'help' => t('Récupère tous les commentaires d\'un noeud'),
          'file' => array(
            'type' => 'inc',
            'module' => 'webservices',
            'name' => 'webservices.resource',
          ),
          'access arguments' => array('access content'),
          'callback' => '_webservices_resource_node_comments',
          'args' => array(
            array(
              'name' => 'nid',
              'type' => 'int',
              'description' => t("The Drupal node's nid."),
              'source' => array('data' => 'nid'),
              'optional' => FALSE,
            ),
          ),
        ),
        'getFavorites' => array(
          'help' => t('Récupère les favoris de l\'utilisateur courant'),
          'file' => array(
            'type' => 'inc',
            'module' => 'webservices',
            'name' => 'webservices.resource',
          ),
          'access arguments' => array('retrieve favoris'),
          'callback' => '_webservices_resource_content_getFavorites',
        ),
        'addFavorites' => array(
          'help' => t('Ajoute le favoris de l\'utilisateur courant'),
          'file' => array(
            'type' => 'inc',
            'module' => 'webservices',
            'name' => 'webservices.resource',
          ),
          'access arguments' => array('retrieve favoris'),
          'callback' => '_webservices_resource_content_addFavorites',
          'args' => array(
            array(
              'name' => 'nid',
              'type' => 'int',
              'description' => t("The Drupal node's nid."),
              'source' => array('data' => 'nid'),
              'optional' => FALSE,
            ),
          ),
        ),
        'deleteFavorites' => array(
          'help' => t('Supprime le favoris de l\'utilisateur courant'),
          'file' => array(
            'type' => 'inc',
            'module' => 'webservices',
            'name' => 'webservices.resource',
          ),
          'access arguments' => array('retrieve favoris'),
          'callback' => '_webservices_resource_content_deleteFavorites',
          'args' => array(
            array(
              'name' => 'nid',
              'type' => 'int',
              'description' => t("The Drupal node's nid."),
              'source' => array('data' => 'nid'),
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),
    /*    'facebook' => array(
          'actions' => array(

          ),
        ),*/
    'twitter' => array(
      'actions' => array(
        'connectUser' => array(
          'help' => t('Connecte l\'utilisateur Twitter.'),
          'file' => array(
            'type' => 'inc',
            'module' => 'webservices',
            'name' => 'webservices.resource',
          ),
          'access arguments' => array('access content'),
          'callback' => '_webservices_resource_twitter_connectUser',
          'args' => array(
            array(
              'name' => 'twitter_id',
              'type' => 'int',
              'description' => t("Twitter ID."),
              'source' => array('data' => 'twitter_id'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'key',
              'type' => 'string',
              'description' => t("Twitter key."),
              'source' => array('data' => 'key'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'secret',
              'type' => 'string',
              'description' => t("Twitter secret."),
              'source' => array('data' => 'secret'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'twitter_username',
              'type' => 'string',
              'description' => t("Twitter username."),
              'source' => array('data' => 'twitter_username'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'twitter_email',
              'type' => 'string',
              'description' => t("Twitter email."),
              'source' => array('data' => 'twitter_email'),
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),
    'user' => array(
      'actions' => array(
        'comments' => array(
          'help' => t('Récupère les commentaires de l\'utilisteur'),
          'file' => array(
            'type' => 'inc',
            'module' => 'webservices',
            'name' => 'webservices.resource',
          ),
          'access arguments' => array('access content'),
          'callback' => '_webservices_resource_current_user_comments',
        ),
        'createlogin' => array(
          'help' => t('Crée et log un user'),
          'file' => array(
            'type' => 'inc',
            'module' => 'webservices',
            'name' => 'webservices.resource',
          ),
          'access arguments' => array('access content'),
          'callback' => '_webservices_resource_current_user_createlogin',
          'args' => array(
            /*            array(
                          'name' => 'type',
                          'type' => 'string',
                          'description' => t("Type de compte."),
                          'source' => array('data' => 'type'),
                          'optional' => FALSE,
                        ),*/
            array(
              'name' => 'account',
              'type' => 'array',
              'description' => t("Account."),
              'source' => 'data',
              'optional' => FALSE,
            ),
            /* array(
               'name' => 'mail',
               'type' => 'int',
               'description' => t("Email."),
               'source' => array('data' => 'mail'),
               'optional' => FALSE,
             ),
             array(
               'name' => 'pass',
               'type' => 'string',
               'description' => t("Twitter key."),
               'source' => array('data' => 'pass'),
               'optional' => FALSE,
             ),
             array(
               'name' => 'name',
               'type' => 'string',
               'description' => t("Twitter secret."),
               'source' => array('data' => 'name'),
               'optional' => FALSE,
             ),*/
          ),
        ),
        'connectFBUser' => array(
          'help' => t('Connecte l\'utilisateur Facebook.'),
          'file' => array(
            'type' => 'inc',
            'module' => 'webservices',
            'name' => 'webservices.resource',
          ),
          'access arguments' => array('access content'),
          'callback' => '_webservices_resource_facebook_connectUser',
          'args' => array(
            array(
              'name' => 'mail',
              'type' => 'int',
              'description' => t("Email."),
              'source' => array('data' => 'mail'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'pass',
              'type' => 'string',
              'description' => t("Twitter key."),
              'source' => array('data' => 'pass'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'name',
              'type' => 'string',
              'description' => t("Twitter secret."),
              'source' => array('data' => 'name'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'fb_id',
              'type' => 'int',
              'description' => t("Facebook ID."),
              'source' => array('data' => 'fb_id'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'fb_firstname',
              'type' => 'string',
              'description' => t("Facebook first name."),
              'source' => array('data' => 'fb_firstname'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'fb_lastname',
              'type' => 'string',
              'description' => t("Facebook lastname."),
              'source' => array('data' => 'fb_lastname'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'fb_username',
              'type' => 'string',
              'description' => t("Facebook username."),
              'source' => array('data' => 'fb_username'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'fb_email',
              'type' => 'string',
              'description' => t("Facebook email."),
              'source' => array('data' => 'fb_email'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'fb_token',
              'type' => 'string',
              'description' => t("Facebook token."),
              'source' => array('data' => 'fb_token'),
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),
  );
  return $webservices_resource;
}
