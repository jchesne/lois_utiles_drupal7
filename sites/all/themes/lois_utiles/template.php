<?php
/*
  Preprocess
*/

/*
function NEWTHEME_preprocess_html(&$vars) {
  //  kpr($vars['content']);
}
*/
function lois_utiles_preprocess_page(&$vars, $hook) {
  //typekit
  //drupal_add_js('http://use.typekit.com/XXX.js', 'external');
  //drupal_add_js('try{Typekit.load();}catch(e){}', array('type' => 'inline'));

  //webfont
  //drupal_add_css('http://cloud.webtype.com/css/CXXXX.css','external');

  //googlefont 
  //  drupal_add_css('http://fonts.googleapis.com/css?family=Bree+Serif','external');

}

/*
function NEWTHEME_preprocess_region(&$vars,$hook) {
  //  kpr($vars['content']);
}
*/
/*
function NEWTHEME_preprocess_block(&$vars, $hook) {
  //  kpr($vars['content']);

  //lets look for unique block in a region $region-$blockcreator-$delta
   $block =  
   $vars['elements']['#block']->region .'-'. 
   $vars['elements']['#block']->module .'-'. 
   $vars['elements']['#block']->delta;
   
  // print $block .' ';
   switch ($block) {
     case 'header-menu_block-2':
       $vars['classes_array'][] = '';
       break;
     case 'sidebar-system-navigation':
       $vars['classes_array'][] = '';
       break;
    default:
    
    break;

   }


  switch ($vars['elements']['#block']->region) {
    case 'header':
      $vars['classes_array'][] = '';
      break;
    case 'sidebar':
      $vars['classes_array'][] = '';
      $vars['classes_array'][] = '';
      break;
    default:

      break;
  }

}
*/
/*
function NEWTHEME_preprocess_node(&$vars,$hook) {
  //  kpr($vars['content']);

  // add a nodeblock 
  // in .info define a region : regions[block_in_a_node] = block_in_a_node 
  // in node.tpl  <?php if($noderegion){ ?> <?php print render($noderegion); ?><?php } ?>
  //$vars['block_in_a_node'] = block_get_blocks_by_region('block_in_a_node');
}
*/
/*
function NEWTHEME_preprocess_comment(&$vars,$hook) {
  //  kpr($vars['content']);
}
*/
/*
function NEWTHEME_preprocess_field(&$vars,$hook) {
  //  kpr($vars['content']);
  //add class to a specific field
  switch ($vars['element']['#field_name']) {
    case 'field_ROCK':
      $vars['classes_array'][] = 'classname1';
    case 'field_ROLL':
      $vars['classes_array'][] = 'classname1';
      $vars['classes_array'][] = 'classname2';
      $vars['classes_array'][] = 'classname1';
    case 'field_FOO':
      $vars['classes_array'][] = 'classname1';
    case 'field_BAR':
      $vars['classes_array'][] = 'classname1';    
    default:
      break;
  }

}
*/
/*
function NEWTHEME_preprocess_maintenance_page(){
  //  kpr($vars['content']);
}
*/
/*
function NEWTHEME_form_alter(&$form, &$form_state, $form_id) {
  //if ($form_id == '') {
  //....
  //}
}
*/

/**
 * Implements hook_page_alter().
 */
function lois_utiles_page_alter(&$page) {

  $url = current_path();
  if ($url == "node/add/soumission-de-lois") {
    _markup_formulaire_soumission_loi($page);
  }
}

function _markup_formulaire_soumission_loi(&$page) {
  global $user;
  if ($user->uid) {
    //User connecté.
    $user_wrapper = entity_metadata_wrapper("user", $user);

    $page["auteur_soumission"] = t("Vous allez soumettre en tant que : @prenom @nom.", array(
      "@prenom" => $user_wrapper->field_membre_prenom->value(),
      "@nom" => substr($user_wrapper->field_membre_nom->value(), 0, 1),
    ));
  }
  else {
    //User déconnecté.
    $page["auteur_soumission"] = "Vous allez soumettre en tant que : <b>Anonyme</b> - " . l("Se connecter", "user/login", array("query" => array("destination" => "node/add/soumission-de-lois")));
  }
  drupal_set_title("Soumission d'une lois");
}

function lois_utiles_theme() {
  $items = array();
  $items['user_register_form'] = array
  (
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'lois_utiles') . '/templates',
    'template' => 'user-register-form',
    'preprocess functions' => array(
      'lois_utiles_preprocess_user_register_form'
    ),
  );
  $items['user_login'] = array
  (
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'lois_utiles') . '/templates',
    'template' => 'user-login-form',
    'preprocess functions' => array(
      'lois_utiles_preprocess_user_login_form'
    ),
  );
  /*  $items['simplenews_block_form_34'] = array
    (
      'render element' => 'form',
      'path' => drupal_get_path('theme', 'gdfsuez') . '/templates',
      'template' => 'user-newsletter-form',
      'preprocess functions' => array(
        'gdfsuez_preprocess_newsletter_form'
      ),
    );
    $items['recettes_node_form'] = array
    (
      'render element' => 'form',
      'path' => drupal_get_path('theme', 'gdfsuez') . '/templates',
      'template' => 'membre-add-recette-form',
      'preprocess functions' => array(
        'gdfsuez_preprocess_add_recette_form'
      ),
    );
    $items['photos_concours_node_form'] = array
    (
      'render element' => 'form',
      'path' => drupal_get_path('theme', 'gdfsuez') . '/templates',
      'template' => 'membre-add-concours-photo-form',
      'preprocess functions' => array(
        'gdfsuez_preprocess_add_concours_photo_form'
      ),
    );*/

  return $items;
}

function lois_utiles_preprocess_user_login_form(&$vars) {
  $form = $vars["form"];
  $vars['title'] = array('#markup' => "<h2 class='heading-2'>Connectez vous avec votre compte Lois Utiles</h2>");
  $form["inscription"] = array(
    '#markup' => "<p style='text-align: center;'>Si vous n'êtes pas déjà inscrit, <a class='purple' href='" . url("user/register", array('absolute' => TRUE)) . "'>inscrivez-vous !</a></p>",
    "#weight" => -10
  );

  $form["forget_password"] = array(
    '#markup' => l("Mot de passe oublié ?", "user/password"),
    "#weight" => 0.002,
  );

  $form["actions"]["submit"]["#value"] = t("Connexion");
  $form["actions"]["submit"]["#attributes"]['class'][] = "btn btn-white";
  $form["actions"]["#weight"] = 300;
  $form["#sorted"] = FALSE;

  unset($form["name"]["#title"]);
  $form["name"]["#attributes"]["placeholder"] = t("Adresse e-mail");
  unset($form["name"]["#description"]);
  unset($form["pass"]["#title"]);
  $form["pass"]["#attributes"]["placeholder"] = t("Mot de passe");
  unset($form["pass"]["#description"]);
  unset($form["twitter_signin"]);
  $vars['children'] = drupal_render_children($form);
}

function lois_utiles_form_alter(&$form, $form_state, $form_id) {
  switch ($form_id) {

    case 'user_pass':
      $form['actions']['submit']['#value'] = t('Envoyer le nouveau mot de passe par email');
      if (isset($form['mail'])) {
        $form['mail']['#markup'] = t("Les instructions de réinitialisation de mot de passe vont être envoyées à !email. Vous devez vous déconnecter pour utiliser le lien de réinitialisation de mot de passe situé dans l'email.", array("!email" => $form['name']["#value"]));
      }
      $form['actions']['submit']['#attributes']['class'][] = "btn btn-white form-submit";
      break;
    case 'user_cancel_confirm_form':
      $form['user_cancel_confirm']['#default_value'] = FALSE;
      $form['user_cancel_confirm']['#value'] = FALSE;
      $form['description']["#markup"] = "Votre compte sera supprimé et ainsi que toutes ses informations. Les recettes et photos ajoutées sur le site seront attribuées au compte Cuisinez au Naturel. Si vous souhaitez que vos contenus soient supprimés également, merci de contacter le webmaster. Cette action est irréversible.";
      $form['actions']['submit']['#value'] = t('Supprimer le compte');
      $form['actions']['cancel']['#attributes']['class'][] = "cancel  btn btn-white";
      $form['actions']['submit']['#attributes']['class'][] = "confirm btn btn-green";
      $form['#submit'][] = 'gdfsuez_user_cancel_form_submit';
      break;
    case 'user_register_form':
      $form['#attributes']['class'][] = "form-inscription form";
      $form['conditions'] = array(
        '#type' => 'checkbox',
        '#title' => t('J\'accepte les conditions générales d\'utilisation'),
        '#required' => TRUE,
        '#default_value' => 0,
      );
      $form['account']['name']['#type'] = 'hidden';
      $form['account']['name']['#value'] = 'email_registration_' . user_password();
      break;
    /*      case 'user_login':
            $form['#attributes']['class'][] = "form-inscription form";
            break;
          case 'user_profile_form':
            $form['#attributes']['class'][] = "form";
            unset($form["field_membre_liste_courses"]);
            unset($form["field_membre_liste_courses_chef"]);
            unset($form["field_membre_auteur_recette_bool"]);
            unset($form["field_membre_recette_chef_fav"]);
            unset($form["field_membre_recette_membre_fav"]);
            unset($form["locale"]);
            unset($form["account"]["name"]);
            unset($form["picture"]['picture_delete']);
            $form['actions']['cancel']['#value'] = t('Supprimer mon compte');
            $form['actions']['submit']['#attributes']['class'][] = "confirm btn btn-green form-submit";

            $form["field_membre_nom"]["#weight"] = -10;
            $form["field_membre_prenom"]["#weight"] = -9;
            $form["account"]["mail"]["#weight"] = -8;
            break;
          case 'simplenews_block_form_34':
            $form['#attributes']['class'][] = "form form-tiny form-newsletter";
            if (user_is_logged_in()) {
              $form['#attributes']['class'][] = "logged";
            }

            break;
          case 'recettes_node_form':
            $form['#attributes']['class'][] = "form-meal form form-ajax";
            $form['#validate'][] = 'add_recette_validation_handler';
            break;
          case 'photos_concours_node_form':
            $form['#attributes']['class'][] = "form-meal form";

            //On récupère le concours actuel.
            $query = new EntityFieldQuery();
            $query->entityCondition('entity_type', 'node')
              ->entityCondition('bundle', 'concours')
              ->fieldCondition('field_concours_actif', 'value', 1)
              ->propertyCondition('status', 1, '=');
            $result = $query->execute();

            $concours_nid = array_keys($result['node']);

            $form['conditions'] = array(
              '#type' => 'checkbox',
              '#title' => t('J\'ai lu et j\'accepte le règlement du concours'),
              '#required' => TRUE
            );

            $form['conditions']["#prefix"] = '<div class="field-wrapper"><label><span class="purple">3.</span> Validez votre participation</label>';
            $form['conditions']["#suffix"] = l("*Voir le règlement du concours", "cgu-concours", array('attributes' => array('target' => '_blank'))) . '</div>';

            $form['field_concours_photo_concours']["und"]["0"]["nid"]["#default_value"] = $concours_nid[0];

            $form['field_concours_nouveau']["und"]["#value"] = 1;

            $form['#validate'][] = 'add_concours_photo_validation_handler';
            break;*/
  }
}


function lois_utiles_preprocess_user_register_form(&$vars) {


  $form = $vars["form"];
  $vars['title'] = array('#markup' => "<h2 class='heading-2'>Créez un compte Lois Utiles</h2>");
  unset($form["account"]["mail"]["#description"]);
  $form["account"]["mail"]["#attributes"]['placeholder'] = t("Adresse e-mail");

  $form['conditions_link'] = array(
    '#markup' => l("*Lire les conditions générales d'utilisation", "node/194", array('attributes' => array('target' => '_blank'))),
  );
  $form["actions"]["submit"]["#value"] = t("S'inscrire");
  $form["actions"]["submit"]["#attributes"]['class'][] = "btn btn-white";
  $form["actions"]["#weight"] = 300;

  unset($form["account"]["pass"]["#description"]);
  unset($form["account"]["pass"]['pass1']["#title"]);
  unset($form["account"]["pass"]['pass2']["#title"]);


  if (ae_detect_ie()) {
    $form["account"]["pass"]['pass1']["#prefix"] = '<div><span>Mot de passe : </span>';
    $form["account"]["pass"]['pass1']["#suffix"] = '</div>';

    $form["account"]["pass"]['pass2']["#prefix"] = '<div><span>Confirmer le mot de passe : </span>';
    $form["account"]["pass"]['pass2']["#suffix"] = '</div>';
  }
  else {
    $form["account"]["pass"]['pass1']["#attributes"]['placeholder'] = t("Mot de passe");
    $form["account"]["pass"]['pass2']["#attributes"]['placeholder'] = t("Confirmer le mot de passe");
  }

  $vars['password'] = render($form["account"]["pass"]);

  $form["#sorted"] = FALSE;

  unset($form["field_membre_nom"]["und"]["0"]["value"]["#title"]);
  $form["field_membre_nom"]["und"]["0"]["value"]["#attributes"]["placeholder"] = t("Nom");
  $vars['nom'] = render($form["field_membre_nom"]);
  unset($form["field_membre_prenom"]["und"]["0"]["value"]["#title"]);
  $form["field_membre_prenom"]["und"]["0"]["value"]["#attributes"]["placeholder"] = t("Prénom");
  $vars['prenom'] = render($form["field_membre_prenom"]);
  unset($form["account"]["mail"]["#theme_wrapper"]);
  unset($form["account"]["mail"]["#title"]);
  $vars['email'] = render($form["account"]["mail"]);
  unset($form["conditions_link"]["#theme_wrapper"]);
  $vars['conditions_link'] = render($form['conditions_link']);


  $vars['children'] = drupal_render_children($form);
}


function ae_detect_ie() {
  if (isset($_SERVER['HTTP_USER_AGENT']) &&
    (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE)
  ) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}