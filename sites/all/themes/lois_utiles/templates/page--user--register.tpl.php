<?php
//kpr(get_defined_vars());
//kpr($theme_hook_suggestions);
//template naming
//page--[CONTENT TYPE].tpl.php
?>
<?php if (theme_get_setting('mothership_poorthemers_helper')) { ?>
  <!-- page.tpl.php -->
<?php } ?>

<?php print $mothership_poorthemers_helper; ?>

<?php include("header.php"); ?>

<div class="page">

  <div role="main" id="main-content">
    <div class="row">
      <h1 class="heading-1">Inscription</h1>
    </div>
    <div class="row box-greenlight">
      <div class="c-6">
        <div class="box-content">
          <h2 class="heading-2">Inscrivez-vous avec Facebook ou Twitter</h2>

          <p>En vous connectant, vous pourrez ajouter les lois en favoris, soumettre des lois utiles et profiter des nouveautés à venir sur Lois Utiles.</p>

          <p>Si vous êtes déjà inscrit,
            <a class="purple" href="<?php echo base_path() . "user/login"; ?>">connectez-vous !</a>
          </p>
          <?php
          print l("S'inscrire avec Facebook", $GLOBALS['base_url'] . "/user/simple-fb-connect");

          print l("S'inscrire avec Twitter", $GLOBALS['base_url'] . "/twitter/redirect");

          ?>
        </div>
      </div>
      <div class="c-12">
			<span class="choice purple">
				ou
			</span>
      </div>
      <div class="c-6">
        <div class="box-content">
          <?php print render($page['content']); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<footer role="contentinfo">
  <?php print render($page['footer']); ?>
</footer>

