<a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $text; ?>" data-via="LoisUtiles" data-count="none" data-hashtags="<?php echo $hashtag; ?>">Tweet</a>
<script>!function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
    if (!d.getElementById(id)) {
      js = d.createElement(s);
      js.id = id;
      js.src = p + '://platform.twitter.com/widgets.js';
      fjs.parentNode.insertBefore(js, fjs);
    }
  }(document, 'script', 'twitter-wjs');</script>