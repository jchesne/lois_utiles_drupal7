<?php

/**
 * @file
 * Service's resources callbacks.
 */

/**
 * Select votes or vote results.
 *
 * @param $entity_id
 *  Nid du noeud à récupérer
 *
 * @return array
 *   An array of vote results matching the criteria.
 */
function _services_votingapi_resource_select_votes($entity_id, $type) {
  // Return selected votes or vote results.
  $criteria = array(
    'entity_id' => $entity_id,
    'entity_type' => $type,
    'value_type' => "option",
    'tag' => "vote",
  );

  return array(
    "success" => TRUE,
    "action" => "setvote",
    "votes" => call_user_func("votingapi_select_results", $criteria, 0),
  );
}

/**
 *
 * Vote for an entity.
 *
 * @param $entity_id
 *   L'id du noeud à voter
 *
 * @param $value
 *   la valeur, 1 ou -1
 *
 * @return array
 *   Result of voting.
 *
 * @see votingapi_set_votes() to learn more.
 */
function _services_votingapi_resource_set_votes($entity_id, $value, $type) {

  $votes = array(
    array(
      "entity_id" => $entity_id,
      "entity_type" => $type,
      "value_type" => "option",
      "value" => $value,
    ),
  );

  foreach ($votes as $key => $vote) {
    if (empty($vote['entity_id'])) {
      return services_error(t('Missing entity ID for vote ') . $key);
    }
    if (empty($vote['value'])) {
      return services_error(t('Missing votes value for vote ') . $key);
    }
  }

  $redirect = FALSE;
  $save = TRUE;

  $widget = new stdClass;
  $widget->delete_vote_on_second_click = 1;


  $criteria = NULL;

  $context = array(
    'redirect' => &$redirect,
    'save' => &$save,
    'criteria' => &$criteria,
    'widget' => $widget,
  );
  // Je fais appel au alter pour gérer la suppression du vote au second vote.
  drupal_alter('rate_vote', $votes, $context);

  if ($save) {
    // Je vote
    votingapi_set_votes($votes, $criteria);
  }

  // Je récupère le nombre de votes.
  return _services_votingapi_resource_select_votes($votes[0]["entity_id"], $type);
}

