<?php
$url = explode("/", request_path());
$url = end($url);
?>
<?php

$mode_edition = FALSE;
if ($user->uid == $elements["#account"]->uid) {
  $mode_edition = TRUE;
}


$class = "";
if ($elements["#account"]->field_membre_auteur_recette_bool["und"]["0"]["value"] == 1) {
  $class = "icon-meal-medium";
}

$member = user_load($elements["#account"]->uid);
$originPath = $member->picture->uri;
$avatarUrl = image_style_url("avatar_chef_top_290_320", $originPath);
?>
<div class="profile"<?php print $attributes; ?>>
  <div class="row">
    <div class="cell-12">
      <div data-offset-top="200" data-spy="affix" class="box-border box-greenlight box-people-highlight-float align-center">
        <div class="text">
          <h2 class="heading-1 small heading-underline">
            <?php $fullname = print render($elements['field_membre_prenom']) . " " . render($elements['field_membre_nom']) ?>
          </h2>
        </div>

        <div class="portrait">
          <div class="responsive-center">
            <img src="<?php echo $avatarUrl; ?>"/>
          </div>
          <?php if ($mode_edition): ?>
            <div class="edit-buttons">
              <a class="edit" href="<?php echo url("user/" . $user->uid . "/edit", array('absolute' => TRUE)); ?>"><span class="icon icon-edit"></span></a>
            </div>
          <?php endif; ?>
          <span class="icon <?php echo $class; ?>"></span>
        </div>
        <?php if ($mode_edition): ?>
          <a class="box-content box-white" href="<?php echo url("mes-listes-de-courses", array('absolute' => TRUE)); ?>">
            Voir ma liste de courses
            <img alt="" src="<?php echo base_path() . path_to_theme() . "/images/px.png" ?>" class="icon icon-shopping">
          </a>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class="row">
    <?php

    /*$blockRecettes = module_invoke('gdfsuez_membre', 'block_view', 'membre_ses_recettes');
    $content_membre_recettes = $blockRecettes['content']["#markup"];

    $blockFavorites = module_invoke('gdfsuez_membre', 'block_view', 'membre_ses_recettes_favorites');
    $content_membre_recettes_favorites = $blockFavorites['content']["#markup"];

    $class = "";

    if ($content_membre_recettes == "" && $content_membre_recettes_favorites != "") {
      $class = "no-padding";
    }*/

    ?>
    <div class="c-9 p-3 p20x35 <?php echo $class; ?>">
      <!--    <?php
      /*      print render($blockRecettes['content']);
            */ ?>
      <?php
      /*      print render($blockFavorites['content']);
            */ ?>
      <?php
      /*      print render($blockConcours['content']);
            */ ?>

      --><?php
      /*      if ($content_membre_recettes == "" && $content_membre_recettes_favorites == "" && $content_membre_participations_concours == "") {
              if ($user->uid == $elements["#account"]->uid) {
                echo "<span class='msg-member'>Vous n'avez pas encore ajouté de recette, " . l("partagez-en une", $GLOBALS['base_url'] . "/ajouter-une-recette") . " dès maintenant !</span>";
              }
              else {
                echo "<span class='msg-member'>Ce membre n'a pas encore ajouté de recette ni participé à un concours photos</span>";
              }
            }
            */ ?>

    </div>